# Teste Técnico

Prova técnica de testes automatizados para a Web do Sicredi.

## Pré-Requisitos:

- JDK8+;
- Maven 3.6.3;
- Google Chrome 81+

## Como Executar

Após clonar o repositório, em um terminal, execute o comando:

```bash
mvn surefire:test
```

Os resultados de execução são exibidos no terminal.

