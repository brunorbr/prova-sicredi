package com.sicredi;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageObject{

    @FindBy(id = "switch-version-select")
    private WebElement versionSelect;

    @FindBy(xpath = "//option[contains(text(), 'V4 Theme')]")
    private WebElement v4_option;

    @FindBy(xpath = "//*[contains(@class,'btn')][1]")
    private WebElement addCustomerButton;

    public HomePage(WebDriver driver){
        super(driver);
    }

    public void clickVersionDropdown(){
        versionSelect.click();
    }

    public void selectV4(){
        v4_option.click();
    }

    public AddCustomerPage clickAddCustomerButton(WebDriver driver){
        addCustomerButton.click();
        return new AddCustomerPage(driver);
    }
}
