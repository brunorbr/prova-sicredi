package com.sicredi;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AddCustomerPage extends PageObject {

    @FindBy(id = "field-customerName")
    private WebElement nameField;

    @FindBy(id = "field-contactLastName")
    private WebElement lastNameField;

    @FindBy(id = "field-contactFirstName")
    private WebElement contactField;

    @FindBy(id = "field-phone")
    private WebElement phoneField;

    @FindBy(id = "field-addressLine1")
    private WebElement addressField1;

    @FindBy(id = "field-addressLine2")
    private WebElement addressField2;

    @FindBy(id = "field-city")
    private WebElement cityField;

    @FindBy(id = "field-state")
    private WebElement stateField;

    @FindBy(id = "field-postalCode")
    private WebElement postalCodeField;

    @FindBy(id = "field-country")
    private WebElement countryField;

    @FindBy(xpath = "//*[@class='chosen-single chosen-default']")
    private WebElement employerDropDown;

    @FindBy(xpath = "//*[@id='field_salesRepEmployeeNumber_chosen']//input")
    private WebElement dropDownField;

    @FindBy(id = "field-creditLimit")
    private WebElement creditLimitField;

    @FindBy(id = "form-button-save")
    private WebElement saveButton;

    @FindBy(xpath = "//*[@id='report-success']/p")
    private WebElement confirmationMessage;

    @FindBy(id = "report-success")
    private WebElement success;

    public AddCustomerPage(WebDriver driver){
        super(driver);
    }

    public void inputFirstName(String firstName){
        nameField.sendKeys(firstName);
    }

    public void inputLastName(String lastName){
        lastNameField.sendKeys(lastName);
    }

    public void inputContactName(String contactName){
        contactField.sendKeys(contactName);
    }

    public void inputPhone(String phone){
        phoneField.sendKeys(phone);
    }

    public void inputAddress1(String addressLine1){
        addressField1.sendKeys(addressLine1);
    }

    public void inputAddress2(String addressLine2){
        addressField2.sendKeys(addressLine2);
    }

    public void inputCity(String city){
        cityField.sendKeys(city);
    }

    public void inputState(String state){
        stateField.sendKeys(state);
    }

    public void inputPostalCode(String postalCode){
        postalCodeField.sendKeys(postalCode);
    }

    public void inputCountry(String country){
        countryField.sendKeys(country);
    }

    public void clickEmployerDropDown(){
        employerDropDown.click();
    }

    public void inputEmployeer(String employer){
        dropDownField.sendKeys(employer + Keys.ENTER);
    }

    public void inputCreditLimit(String value){
        creditLimitField.sendKeys(value);
    }

    public void clickSaveButton(){
        saveButton.click();
    }

    public String getConfirmationMessage(){
        return success.getAttribute("textContent");
    }
}
