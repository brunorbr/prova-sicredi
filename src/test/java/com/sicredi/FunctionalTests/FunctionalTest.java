package com.sicredi.FunctionalTests;

import com.sicredi.AddCustomerPage;
import com.sicredi.HomePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class FunctionalTest {

    public static WebDriver driver;
    public HomePage home;
    public AddCustomerPage addCustomer;

    public static void wait(int seconds){
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @BeforeAll
    public static void setUp(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions opts = new ChromeOptions();
        opts.addArguments("--start-maximized");
        driver = new ChromeDriver(opts);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://www.grocerycrud.com/demo/bootstrap_theme");
    }

    @AfterAll
    public static void tearDown(){
        driver.manage().deleteAllCookies();
        driver.close();
    }

    @BeforeEach
    public void addCustomer(){
        home = new HomePage(driver);
        home.clickVersionDropdown();
        home.selectV4();
        addCustomer = home.clickAddCustomerButton(driver);
        addCustomer.inputFirstName("Teste Sicredi");
        addCustomer.inputLastName("Teste");
        addCustomer.inputContactName("Bruno Teixeira");
        addCustomer.inputPhone("51 9999-9999");
        addCustomer.inputAddress1("Av Assis Brasil, 3970");
        addCustomer.inputAddress2("Torre D");
        addCustomer.inputCity("Porto Alegre");
        addCustomer.inputState("RS");
        addCustomer.inputPostalCode("91000-000");
        addCustomer.inputCountry("Brasil");
        addCustomer.clickEmployerDropDown();
        addCustomer.inputEmployeer("Fixter");
        addCustomer.inputCreditLimit("200");
        addCustomer.clickSaveButton();
    }

    @Test
    public void ChallengeOne(){
        wait(5);
        Assertions.assertEquals("Your data has been successfully stored into the database. Edit Customer or Go back to list",
                addCustomer.getConfirmationMessage());
    }
}
